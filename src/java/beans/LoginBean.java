package beans;
 
import dao.UserDao;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
 
@ManagedBean(name = "loginBean")
@SessionScoped
/**
 *
 * @author User
 */
public class LoginBean implements Serializable {
 
    private static final long serialVersionUID = 1L;
    private String password;
    private String message, uname;
    
    public String getMessage() {
        return message;
    }
 
    public void setMessage(String message) {
        this.message = message;
    }
 
    public String getPassword() {
        return password;
    }
 
    public void setPassword(String password) {
        this.password = password;
    }
 
    public String getUname() {
        return uname;
    }
 
    public void setUname(String uname) {
        this.uname = uname;
    }
    
    public String signProject(){
       
        return "sign?faces-redirect=true";
    }
    public String sign(){
         UserDao.sign(uname, password);
         return "login?faces-redirect=true";
    }
 
    public String loginProject() {
        
        boolean result = UserDao.login(uname, password);
       
        if (result) {
            // get Http Session and store username
            HttpSession session = Util.getSession();
            session.setAttribute("username",uname);
            System.out.println(session.getAttribute("username"));
            return "home?faces-redirect=true";
        } 
        
        
        
        else {
 
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                    "Yanlis Giris!",
                    "Tekrar Deneyin !!"));
 
            // invalidate session, and redirect to other pages
 
            //message = "Invalid Login. Please Try Again!";
            return "login";
        }
    }
   
    public String logout() {
      HttpSession session = Util.getSession();
      session.invalidate();
      return "login?faces-redirect=true";
   }
}